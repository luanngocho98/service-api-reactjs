import React, { Fragment, useEffect, useState } from 'react'
import { createCategory, deleteCategoryById, editCategoryById, getCategoriesList, getDetailCategoryById } from './services/category';

export default function App() {
  const [categories, setCategories] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const [isLoadingCreate, setIsLoadingCreate] = useState(false);
  const [categoryName, setCategoryName] = useState('');
  const [categoryIdEdit, setCategoryIdEdit] = useState(null);

  useEffect(() => {
    getCategoriesList(
      handleSuccessCategory,
      handleErrorCategory,
      handleFinalyCategory,
    );
  }, []);

  const handleSuccessCategory = (success) => {
    const { data } = success;
    setCategories(data);
  };

  const handleErrorCategory = (error) => {};

  const handleFinalyCategory = () => {
    setIsLoading(false);
  };

  const change = ({ target }) => {
    const { value } = target;
    setCategoryName(value);
  };

  const submit = () => {
    setIsLoadingCreate(true);
    const payload = {
      title: categoryName,
      author: 'me',
    };
    if(categoryIdEdit) {
      editCategoryById(
        categoryIdEdit,
        JSON.stringify(payload),
        handleSuccessCreateCategory,
        handleErrorCreateCategory,
        handleFinalyCreateCategory,
      )
    } else {
      createCategory(
        JSON.stringify(payload),
        handleSuccessCreateCategory,
        handleErrorCreateCategory,
        handleFinalyCreateCategory,
      )
    }
  };

  const handleSuccessCreateCategory = (success) => {
    getCategoriesList(
      handleSuccessCategory,
      handleErrorCategory,
      handleFinalyCategory,
    );
  };

  const handleErrorCreateCategory = (error) => {};

  const handleFinalyCreateCategory = () => {
    setIsLoadingCreate(false);
    setCategoryName('');
  };

  const handleDelete = (categoryId) => {
    deleteCategoryById(
      categoryId,
      handleSuccessDeleteCategory,
      handleErrorDeleteCategory,
      handleFinalyDeleteCategory,
    );
  };

  const handleSuccessDeleteCategory = (success) => {
    getCategoriesList(
      handleSuccessCategory,
      handleErrorCategory,
      handleFinalyCategory,
    );
  };

  const handleErrorDeleteCategory = (error) => {};

  const handleFinalyDeleteCategory = () => {
  };

  const handleEdit = (categoryId) => {
    getDetailCategoryById(
      categoryId,
      (success) => {
        const { data } = success;
        setCategoryName(data.title);
        setCategoryIdEdit(data.id);
      }
    )
  };

  return (
    <Fragment>
      <div style={{ display: 'flex', justifyContent: 'space-evenly' }}>
        {
          isLoading ? (
            <h1>Loading ...</h1>
          ) : (
            categories.map(category => (
              <div key={category.id}>
                {category.title}
                <button onClick={() => handleDelete(category.id)}>X</button>
                <button onClick={() => handleEdit(category.id)}>V</button>
              </div>
            ))
          )
        }
      </div>
      <h2>
        Form Create Category
      </h2>
      <div>
        <input
          value={categoryName}
          onChange={change}
          placeholder='Please enter category name ?'
        />
        <button
          onClick={submit}
          disabled={isLoadingCreate}
        >
          {
            categoryIdEdit ? 'Edit' : 'Create'
          }
        </button>
      </div>
    </Fragment>
  )
}
