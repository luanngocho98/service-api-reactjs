
import axios from "axios";
import { CATEGORIES_URL } from "../constant";

// request Headers
const configAxios = (isFormData = false) => {
  let contentType = 'application/json';
  if(isFormData) {
    contentType = 'multipart/form-data'
  }
  const headers = {
    headers: {
      'Content-Type': contentType
    }
  };

  return headers;
};

// Get all categories
export const getCategoriesList = (responseCb, errorcb, finalCb) => {
  axios.get(CATEGORIES_URL)
    .then(responseCb)
    .catch(errorcb)
    .finally(finalCb)
};

// Create category
export const createCategory = (data, responseCb, errorcb, finalCb) => {
  axios.post(CATEGORIES_URL, data, configAxios())
    .then(responseCb)
    .catch(errorcb)
    .finally(finalCb)
};

// Delete category
export const deleteCategoryById = (categoryId, responseCb, errorcb, finalCb) => {
  axios.delete(`${CATEGORIES_URL}/${categoryId}`)
    .then(responseCb)
    .catch(errorcb)
    .finally(finalCb)
}

// Edit category
export const editCategoryById = (categoryId, data, responseCb, errorcb, finalCb) => {
  axios.put(`${CATEGORIES_URL}/${categoryId}`, data, configAxios())
    .then(responseCb)
    .catch(errorcb)
    .finally(finalCb)
}

// Get detail category by id
export const getDetailCategoryById = (categoryId, responseCb, errorcb, finalCb) => {
  axios.get(`${CATEGORIES_URL}/${categoryId}`)
    .then(responseCb)
    .catch(errorcb)
    .finally(finalCb)
}
